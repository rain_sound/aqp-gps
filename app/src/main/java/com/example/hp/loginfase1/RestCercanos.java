package com.example.hp.loginfase1;


import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;



/**
 * A simple {@link Fragment} subclass.
 */
public class RestCercanos extends Fragment implements OnMapReadyCallback {
    TextView textView;
    LatLng gps_localizacion_actual;
    double radio_tierra;
    MapView mMapView;
    private GoogleMap googleMap1;
    private boolean gps_first = true;
    private MarkerOptions options = new MarkerOptions();
    int height = 100;
    int width = 100;
    public RestCercanos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rest_cercanos, container, false);
        locationStart();
        radio_tierra = 6371.01;

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
       mMapView.getMapAsync(this);
        return view;
    }

    private void obtener_restaurantes_cercanos(boolean meridian, double f1, double f2, double f3, double f4, double f5, double f6, double f7, double f8) {
        String categoria = Path.OBTENER_RESTAURANTES_CERCANOS+"?mer="+meridian+"&"+"f1="+f1+"&"+"f2="+f2+"&"+"f3="+f3+"&"+"f4="+f4+"&"+"f5="+f5+"&"+"f6="+f6+"&"+"f7="+f7+"&"+"f8="+f8;
        List<ListRest> list = new ArrayList<ListRest>();
        ListRest obj ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, categoria,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                datosJson2(response);

            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        });
        Singleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void datosJson2(JSONObject response) {
        List<ListRest> cercanos = new ArrayList();
        JSONArray jsonarray = null;
        try{
            jsonarray = response.getJSONArray("cercanos");
            for(int i = 0; i < jsonarray.length() ; i++){
                try{
                    JSONObject objeto = jsonarray.getJSONObject(i);
                    ListRest rest = new ListRest();
                    rest.setNombre_res(objeto.getString("nom_res"));
                    rest.setDescrip_res(objeto.getString("desc_res"));
                    rest.setName_img(objeto.getString("img_res"));
                    rest.setLat(objeto.getDouble("lat_res"));
                    rest.setLng(objeto.getDouble("lng_res"));
                    cercanos.add(rest);
                }
                catch (JSONException e){

                }
            }
         //tenemos todos los restaurantes en la lista


            for(ListRest obj : cercanos ){
                options.position(new LatLng(obj.getLat(),obj.getLng()));
                options.title(obj.getNombre_res());
                options.snippet("Restaurante");

                    BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.mipmap.rest);
                    Bitmap b=bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                    options.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

                googleMap1.addMarker(options);
            }

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }


    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity((FragmentParent)getContext());
        //verifica si el servicio o proveedor esta activo
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            //si no esta activo envía a configuración del telefono
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }//si esta activo el servicio, verifica los permisos
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }





    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        FragmentParent mainActivity;
        public double lng;
        public double lat;

        public Localizacion(){}
        public double getLat(){return lat;}
        public double getLong(){return lng;}
        public FragmentParent getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(FragmentParent mainActivity) {
            this.mainActivity = mainActivity;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.mipmap.person);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            gps_localizacion_actual = new LatLng(loc.getLatitude(),loc.getLongitude());
            if(gps_first){
                LatLng coordinate = new LatLng(gps_localizacion_actual.latitude, gps_localizacion_actual.longitude);
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(coordinate,15);
                googleMap1.animateCamera(location);

                googleMap1.addMarker(new MarkerOptions().position(coordinate).title("Actual").icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                enviar_solicitud();
                gps_first = false;
            }
        }



        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado

        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }

    //control del mapa
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap1 = googleMap;
    }


    public void enviar_solicitud(){
        double latitud = Math.toRadians(gps_localizacion_actual.latitude);
        double longitud = Math.toRadians(gps_localizacion_actual.longitude);
        GeoLocation location = GeoLocation.fromRadians(latitud,longitud);
        double distancia_km=0.15; //150 metros
        GeoLocation[] boundingCoordinates =  location.boundingCoordinates(distancia_km, radio_tierra);
        boolean meridian180WithinDistance =	boundingCoordinates[0].getLongitudeInRadians() > boundingCoordinates[1].getLongitudeInRadians();
        //este obtener se llama cuando se cargo el mapa previamente
        obtener_restaurantes_cercanos(meridian180WithinDistance,boundingCoordinates[0].getLatitudeInRadians(),
                boundingCoordinates[1].getLatitudeInRadians(),boundingCoordinates[0].getLongitudeInRadians(),
                boundingCoordinates[1].getLongitudeInRadians(),location.getLatitudeInRadians()
                ,location.getLatitudeInRadians(),location.getLongitudeInRadians(),distancia_km / radio_tierra);
    }



}
