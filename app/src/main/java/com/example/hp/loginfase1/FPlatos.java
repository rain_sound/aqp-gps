package com.example.hp.loginfase1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FPlatos extends Fragment {
    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    public FPlatos( ) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fplatos, container, false);

        return view;
    }

    private void inicializar() {
        listDataHeader =  new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Promociones");
        listDataHeader.add("Entradas");
        listDataHeader.add("Postres");
        listDataHeader.add("Especiales");

        List<String> promociones = new ArrayList<>();
        promociones.add("2 x 1 en Entradas");
        promociones.add("Sábados 2 x 1");

        List<String> entradas = new ArrayList<>();
        entradas.add("Piqueo 1");
        entradas.add("Piqueo 2");
        entradas.add("Piqueo 3");

        List<String> postres = new ArrayList<>();
        postres.add("Torta");
        postres.add("Helado");

        List<String> especiales = new ArrayList<>();
        especiales.add("Pizza de la casa");
        especiales.add("Otro plato");

        listHash.put(listDataHeader.get(0),promociones);
        listHash.put(listDataHeader.get(1),entradas);
        listHash.put(listDataHeader.get(2),postres);
        listHash.put(listDataHeader.get(3),especiales);
    }

}
