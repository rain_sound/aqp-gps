package com.example.hp.loginfase1;


import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Tab2 extends Activity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab2);

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        inicializar();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }


    private void inicializar() {
        listDataHeader =  new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Promociones");
        listDataHeader.add("Entradas");
        listDataHeader.add("Postres");
        listDataHeader.add("Especiales");

        List<String> promociones = new ArrayList<>();
        promociones.add("2 x 1 en Entradas");
        promociones.add("Sábados 2 x 1");

        List<String> entradas = new ArrayList<>();
        entradas.add("Piqueo 1");
        entradas.add("Piqueo 2");
        entradas.add("Piqueo 3");

        List<String> postres = new ArrayList<>();
        postres.add("Torta");
        postres.add("Helado");

        List<String> especiales = new ArrayList<>();
        especiales.add("Pizza de la casa");
        especiales.add("Otro plato");

        listHash.put(listDataHeader.get(0),promociones);
        listHash.put(listDataHeader.get(1),entradas);
        listHash.put(listDataHeader.get(2),postres);
        listHash.put(listDataHeader.get(3),especiales);
    }
}
