package com.example.hp.loginfase1;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.w3c.dom.Text;

public class FragmentParent extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{
    TextView nombre;
    TextView direccion;
    NetworkImageView img1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_parent);
        Intent intent = getIntent();
        Usuarios usuarios = (Usuarios)intent.getSerializableExtra("usuario");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //accion del btn flotante
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Lleva a una nueva actividad por aquí", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        //Layout hijo
        TextView nombre = (TextView)headerLayout.findViewById(R.id.direccion_user);
        img1 = (NetworkImageView)headerLayout.findViewById(R.id.imgView1);
        nombre.setText(usuarios.getDireccion());

        String ruta = Path.IMAGE +"user.png";
         ImageLoader imageLoader = Singleton.getInstance(this).getImageLoader();
         img1.setImageUrl(ruta,imageLoader);
         cargarFragment();

    }

    private void cargarFragment() {
        android.support.v4.app.Fragment fr = new TabsRestaurante();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fr).commit();

        getSupportActionBar().setTitle("Principal");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                this.finish();

            } else {
                getFragmentManager().popBackStack();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this,"hola",Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean fragmentTransaction = false;
        android.support.v4.app.Fragment fr = null;
        if (id == R.id.nav_profile) {
            fr = new profile();
            fragmentTransaction  = true;
        } else if (id == R.id.nav_gallery) {
            fr = new FRestaurante();
            fragmentTransaction  = true;

        } else if (id == R.id.nav_slideshow) {
            fr = new FRestaurante();
            fragmentTransaction  = true;
        } else if (id == R.id.nav_manage) {

        }
        if(fragmentTransaction) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fr).addToBackStack(null).commit();
            item.setChecked(true);
            getSupportActionBar().setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
