package com.example.hp.loginfase1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Choise extends AppCompatActivity {

    Button res;
    Button user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choise);
        user = (Button)findViewById(R.id.btn_user);
        res = (Button)findViewById(R.id.btn_rest);
    }

    public void onClickU(View view){
        startActivity(new Intent(this,RegisterActivity.class));
    }
    public void onClickR(View view){
        startActivity(new Intent(this,RegistRest.class));
    }

}

