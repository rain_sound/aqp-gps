package com.example.hp.loginfase1;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class FRestaurante extends Fragment implements AdapterView.OnItemSelectedListener{
    private Spinner categories;
    private List<ListRest> image_details; //lista de almuerzos
    private ListView listView;
    private static final String TAG = "PostAdapter";
    private int pos_ant;
    private ViewPager viewPager;
    android.support.v4.app.Fragment fr;
    public FRestaurante() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        pos_ant = -1;
        View view = inflater.inflate(R.layout.fragment_frestaurante, container, false);
        this.categories = (Spinner)view.findViewById(R.id.categories_spinner);
        //Cargar categorías spinner

        load_categories();
        listView = (ListView) view.findViewById(R.id.listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                ListRest click = (ListRest)a.getItemAtPosition(position);
                if(fr==null){
                    fr = new FragmentMenu();
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable("restaurante",click);
                fr.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.content_main, fr).addToBackStack(null).commit();
            }
        });


        return view;
    }


    //objetos a cargar para cada categoria
    //seria una consulta por categoria wbservice-consulta por categoria
    //NECESITA WEBSERVICE
    private  void getListData(int pos) {
        if(pos_ant == pos)return;
        pos_ant = pos;
        String categoria = Path.OBTENER_RESTAURANTES_CATEGORIA+"?cat="+(pos+3);
        List<ListRest> list = new ArrayList<ListRest>();
        ListRest obj ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, categoria,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                datosJson(response);

            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        });
        Singleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void datosJson(JSONObject response) {
        List<ListRest> restaurantes = new ArrayList();
        JSONArray jsonarray = null;
        try{
            jsonarray = response.getJSONArray("restaurantes");
            for(int i = 0; i < jsonarray.length() ; i++){
                try{
                    JSONObject objeto = jsonarray.getJSONObject(i);
                    ListRest rest = new ListRest();
                    rest.setNombre_res(objeto.getString("nom_res"));
                    rest.setName_img(objeto.getString("img_res")+".jpg");
                    rest.setDescrip_res(objeto.getString("desc_res"));
                    rest.setCalificacion(objeto.getInt("cali_res"));
                    rest.setLat(objeto.getDouble("lat_res"));
                    rest.setLng(objeto.getDouble("lng_res"));
                    rest.setCod(objeto.getInt("cod_rest"));


                    restaurantes.add(rest);
                }
                catch (JSONException e){
                    Log.e(TAG, "Error de parsing: "+ e.getMessage());
                }
            }
            listView.setAdapter(new CustomListAdapter(getContext(), restaurantes));
        }
        catch (JSONException e){
            e.printStackTrace();
        }

    }

    //==========================================================================

    //las categorias ya estan definidas no se necesitan recuperar
    private void load_categories() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),R.array.categories,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.categories.setAdapter(adapter);
        this.categories.setOnItemSelectedListener(this);
    }

    //carga restaurantes por categorias
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        //una vez cargadas las categorias, se cargan todos los restaurantes pertenecientes a estas categorias
        //entonces la forma en que se haría sería algo como
        getListData(pos);


    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



}
