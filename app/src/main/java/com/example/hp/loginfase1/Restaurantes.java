package com.example.hp.loginfase1;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.widget.Toast;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Restaurantes extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    private Spinner categories;
    private List<ListRest> image_details; //lista de almuerzos
    private ListView listView;
    private static final String TAG = "PostAdapter";
    private int pos_ant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant);
        pos_ant = -1;
        this.categories = (Spinner)findViewById(R.id.categories_spinner);
        //Cargar categorías spinner
        load_categories();
        listView = (ListView) findViewById(R.id.listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                ListRest click = (ListRest)a.getItemAtPosition(position);
                Intent intent = new Intent("com.example.Menu");
                intent.putExtra("restaurante",click);
                startActivity(intent);
            }
        });

    }

    //==========================================================================

    //objetos a cargar para cada categoria
    //seria una consulta por categoria wbservice-consulta por categoria
    //NECESITA WEBSERVICE
    private  void getListData(int pos) {
        if(pos_ant == pos)return;
        pos_ant = pos;
        String categoria = Path.OBTENER_RESTAURANTES_CATEGORIA+"?cat="+pos;
        List<ListRest> list = new ArrayList<ListRest>();
        ListRest obj ;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, categoria,null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                datosJson(response);

            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        });
        Singleton.getInstance(getBaseContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void datosJson(JSONObject response) {
        List<ListRest> restaurantes = new ArrayList();
        JSONArray jsonarray = null;
        try{
            jsonarray = response.getJSONArray("restaurantes");
            for(int i = 0; i < jsonarray.length() ; i++){
                try{
                    JSONObject objeto = jsonarray.getJSONObject(i);
                    ListRest rest = new ListRest();
                    rest.setNombre_res(objeto.getString("nom_res"));
                    rest.setName_img(objeto.getString("img_res"));
                    rest.setDescrip_res(objeto.getString("desc_res"));
                    rest.setCalificacion(objeto.getInt("cali_res"));
                    rest.setLat(objeto.getDouble("lat_res"));
                    rest.setLng(objeto.getDouble("lng_res"));
                    rest.setCod(objeto.getInt("cod_rest"));


                    restaurantes.add(rest);
                }
                catch (JSONException e){
                    Log.e(TAG, "Error de parsing: "+ e.getMessage());
                }
            }
            listView.setAdapter(new CustomListAdapter(this, restaurantes));
        }
        catch (JSONException e){
            e.printStackTrace();
        }

    }

    //==========================================================================

    //las categorias ya estan definidas no se necesitan recuperar
    private void load_categories() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.categories,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.categories.setAdapter(adapter);
        this.categories.setOnItemSelectedListener(this);
    }

    //carga restaurantes por categorias
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        //una vez cargadas las categorias, se cargan todos los restaurantes pertenecientes a estas categorias
        //entonces la forma en que se haría sería algo como
        getListData(pos);


    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
