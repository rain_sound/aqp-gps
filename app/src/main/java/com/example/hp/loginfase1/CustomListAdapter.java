package com.example.hp.loginfase1;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

public class CustomListAdapter extends BaseAdapter {
    private List<ListRest> listData;
    private LayoutInflater layoutInflater;
    private Context context;
    private ItemClickListener listener;
    public List<ListRest> getData(){return listData;}

    public CustomListAdapter(Context aContext,  List<ListRest> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }
    @Override
    public int getCount() {
        return listData.size();
    }
    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_rest, null);
            holder = new ViewHolder();
            holder.image_logo = (NetworkImageView) convertView.findViewById(R.id.image_view);
            holder.name_rest = (TextView) convertView.findViewById(R.id.txt_name);
            holder.desc_rest = (TextView) convertView.findViewById(R.id.txt_descrip);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ListRest restaurant = this.listData.get(position);
        holder.name_rest.setText(restaurant.getNombre_res());
        holder.desc_rest.setText(restaurant.getDescrip_res());

        ImageLoader imageLoader = Singleton.getInstance(context).getImageLoader();
        holder.image_logo.setImageUrl(Path.IMAGE+ restaurant.getName_img(),imageLoader);

        return convertView;
    }




    static class ViewHolder implements View.OnClickListener{
        NetworkImageView image_logo;
        TextView name_rest;
        TextView desc_rest;
        public ItemClickListener listener;

        public ViewHolder(){}

        public ViewHolder(View v,ItemClickListener listener){
            this.listener = listener;
            v.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
           // listener.onItemClick(v, getAdapterPosition());
        }
    }
    public void setClickListener(ItemClickListener item){
        listener = item;
    }

    interface ItemClickListener{
        void onItemClick(View view,int position);
    }

}


