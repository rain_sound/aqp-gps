package com.example.hp.loginfase1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;




import org.json.JSONObject;

import java.util.HashMap;


public class RegistRest extends AppCompatActivity  {
    Button btn;
    Button pickerButton;
    private static final int PLACE_PICKER_REQUEST = 1;


    EditText nom;
    EditText dire;
    EditText pass;
    EditText des;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist_rest);
        btn = (Button)findViewById(R.id.insertar);
        nom = (EditText)findViewById(R.id.txtnombreR);
        dire = (EditText)findViewById(R.id.txtEmailR);
        pass = (EditText)findViewById(R.id.txtPasswordR);
        des = (EditText)findViewById(R.id.txtDescrR);
        pickerButton = (Button) findViewById(R.id.localizacion);
    }
    public void localizacion(View view){
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(RegistRest.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }

       /* if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final LatLng lat = place.getLatLng();


        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }*/
    }


    public void onClickInsertar(View view){

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("correo",dire.getText().toString());
        params.put("lat","-16.4087521");
        params.put("lng","-71.5360656");
        params.put("nom", nom.getText().toString());
        params.put("img","default.png" );
        params.put("cat","1");
        params.put("pass",pass.getText().toString());
        params.put("desc",des.getText().toString());
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest(
                Request.Method.POST,
                Path.INSERT_RESTAURANTES,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Manejo de la respuesta
                        //notifyDataSetChanged();
                        Toast.makeText(getBaseContext(),"Registro Exitoso",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getBaseContext(),FragmentParent.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(),error.toString(),Toast.LENGTH_LONG).show();

                    }
                });
        Singleton.getInstance(getBaseContext()).addToRequestQueue(jsArrayRequest);
    }
}
