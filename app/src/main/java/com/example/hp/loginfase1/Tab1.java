package com.example.hp.loginfase1;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Tab1 extends AppCompatActivity implements OnMapReadyCallback{
    ListRest restaurante;
    TextView txtName;
    TextView txtDes;
    NetworkImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab1);
        Intent response = getIntent();
        restaurante = (ListRest)response.getSerializableExtra("selected");
        txtName = (TextView)findViewById(R.id.txtNombre);
        txtDes = (TextView)findViewById(R.id.txtDesc);
        img = (NetworkImageView)findViewById(R.id.imgView);
        SupportMapFragment fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);
        cargar_datos();
    }

    private void cargar_datos() {
        txtName.setText(restaurante.getNombre_res());
        txtDes.setText(restaurante.getDescrip_res());
        ImageLoader imageLoader = Singleton.getInstance(this).getImageLoader();
        img.setImageUrl(Path.IMAGE+ restaurante.getName_img(),imageLoader);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng coordinate = new LatLng(restaurante.getLat(), restaurante.getLng());
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(coordinate,15);
        googleMap.animateCamera(location);
        googleMap.addMarker(new MarkerOptions().position(coordinate).title(restaurante.getName_img()));

    }
}
