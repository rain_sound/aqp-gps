package com.example.hp.loginfase1;


import android.app.TabActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TabHost;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@SuppressWarnings("deprecation")
public class Menu extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);

        TabHost.TabSpec tab1 = tabHost.newTabSpec("Restaurante");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("Menú");
        Intent re = getIntent();
        ListRest selected = (ListRest)re.getSerializableExtra("restaurante");
        tab1.setIndicator("RESTAURANTE");
        Intent send = new Intent(this, Tab1.class);
        send.putExtra("selected",selected);
        tab1.setContent(send);
        Intent send2 = new Intent(this, Tab2.class);
        tab2.setIndicator("MENÚ");
        tab2.setContent(send2);

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
    }
}
