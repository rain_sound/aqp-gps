package com.example.hp.loginfase1;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMenu extends Fragment {

    private AppBarLayout appBar;
    private TabLayout pestanas;
    private ViewPager viewPager;
    private Bundle bundle;
    private ListRest listrest;
    public FragmentMenu() {}

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        appBar.removeView(pestanas);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_menu, container, false);
        bundle =getArguments();
         listrest = (ListRest)bundle.getSerializable("restaurante");
        if(savedInstanceState == null){
            insertarTabs(container);
            viewPager = (ViewPager) view.findViewById(R.id.pager1);
            poblarViewPager(viewPager);
            pestanas.setupWithViewPager(viewPager);
        }
        return view;
    }

    public class AdaptadorSecciones extends FragmentStatePagerAdapter {
        private final List<Fragment> fragmentos = new ArrayList<>();
        private final List<String> titulosFragmentos = new ArrayList<>();

        public AdaptadorSecciones(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return fragmentos.get(position);
        }

        @Override
        public int getCount() {
            return fragmentos.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            fragmentos.add(fragment);
            titulosFragmentos.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titulosFragmentos.get(position);
        }
    }

    private void insertarTabs(ViewGroup container) {
        View padre = (View) container.getParent();
        appBar = (AppBarLayout)padre.findViewById(R.id.appbar);
        pestanas = new TabLayout(getActivity());
        pestanas.setTabTextColors(Color.parseColor("#FFFFFF"),Color.parseColor("#FFFFFF"));
        appBar.addView(pestanas);

    }

    private void poblarViewPager(ViewPager viewPager) {
        FragmentMenu.AdaptadorSecciones adapter = new FragmentMenu.AdaptadorSecciones(getFragmentManager());
        Fragment fdescripcion = new FDescripcion();
        Fragment fplatos = new FPlatos();
        Bundle bundle1 = new Bundle();
        bundle1.putSerializable("restaurante",listrest);
        fdescripcion.setArguments(bundle1);
        fplatos.setArguments(bundle1);
        adapter.addFragment(fdescripcion, "Descripción");
        adapter.addFragment(fplatos, "Platos");
        viewPager.setAdapter(adapter);
    }

}
