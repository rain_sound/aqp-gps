package com.example.hp.loginfase1;


public  class Path {
    public static String PATH="https://restaurantaqpdv.000webhostapp.com/webservices/";
    public static String IMAGE="https://restaurantaqpdv.000webhostapp.com/uploads/";
    public static String INSERT_USUARIOS = "insert_usuarios.php";
    public static String INSERT_RESTAURANTES = PATH+"insert_restaurantes.php";
    public static String OBTENER_USUARIOS = PATH+"obtener_usuarios.php";
    public static String OBTENER_USUARIOS_ID = "";
    public static String OBTENER_RESTAURANTES = "obtener_restaurantes.php";
    public static String OBTENER_RESTAURANTES_CERCANOS = PATH+"obtener_restaurantes_cercanos.php";
    public static String OBTENER_RESTAURANTES_CATEGORIA =PATH+"obtener_res_cat.php";
}
