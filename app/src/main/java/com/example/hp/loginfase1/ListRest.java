package com.example.hp.loginfase1;

import java.io.Serializable;

public class ListRest implements Serializable{
    private int cod;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    private String nombre_res;
    private String name_img;
    private String descrip_res;
    private int categoría;
    private double lat;
    private double lng;

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {

        return Math.toDegrees(lat);
    }

    public double getLng() {

        return Math.toDegrees(lng);
    }

    private int calificacion;

    public ListRest(String name, String img, String desc, int calificacion){
        this.nombre_res = name;
        this.name_img = img;
        this.descrip_res = desc;
        this.calificacion = calificacion;
    }
    public ListRest(){}

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public void setCategoría(int categoría) {
        this.categoría = categoría;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public int getCategoría() {
        return categoría;
    }

    public String getNombre_res(){return nombre_res;}
    public String getName_img(){return name_img;}
    public String getDescrip_res(){return descrip_res;}

    public void setNombre_res(String name){this.nombre_res=name;}
    public void setName_img(String img){this.name_img=img;}
    public void setDescrip_res(String desc){this.descrip_res=desc;}
}
