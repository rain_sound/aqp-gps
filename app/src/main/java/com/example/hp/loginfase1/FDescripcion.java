package com.example.hp.loginfase1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FDescripcion extends Fragment implements OnMapReadyCallback {

    ListRest restaurante;
    TextView txtName;
    TextView txtDes;
    NetworkImageView img;

    public FDescripcion( ) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fdescripcion, container, false);
        Bundle bundle = getArguments();
        restaurante = (ListRest)bundle.getSerializable("restaurante");
        txtName = (TextView)view.findViewById(R.id.txtNombre);
        txtDes = (TextView)view.findViewById(R.id.txtDesc);
        img = (NetworkImageView)view.findViewById(R.id.imgView);
        FragmentParent con = (FragmentParent)getActivity();
        //RECUERDA USAR getChildFragmentManager POR FAVOR TE LO RUEGO TE LO IMPLORO CON FRAGMENTOS
        SupportMapFragment  fragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map1);;
        fragment.getMapAsync(this);
        cargar_datos();
        return view;
    }

    private void cargar_datos() {
        txtName.setText(restaurante.getNombre_res());
        txtDes.setText(restaurante.getDescrip_res());
        ImageLoader imageLoader = Singleton.getInstance(getContext()).getImageLoader();
        img.setImageUrl(Path.IMAGE+ restaurante.getName_img(),imageLoader);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng coordinate = new LatLng(restaurante.getLat(), restaurante.getLng());
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(coordinate,15);
        googleMap.animateCamera(location);
        googleMap.addMarker(new MarkerOptions().position(coordinate).title(restaurante.getName_img()));

    }

}
