package com.example.hp.loginfase1;

import java.io.Serializable;

/**
 * Created by Ramiro on 16/05/2017.
 */

public class Usuarios implements Serializable {
    private String nombre;
    private String direccion;
    private String contraseña;
    private String imagen;
    private String codigo;

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getImagen() {
        return imagen;
    }

    public String getCodigo() {
        return codigo;
    }


    public Usuarios(){}
    public Usuarios(String nombre, String direccion, String contraseña, String imagen) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.contraseña = contraseña;
        this.imagen = imagen;

    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
